/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e){
        System.out.println("MyActionListener: Action");
    }
}
/**
 *
 * @author User
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(400, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourName = new JLabel("Your Name:");
        lblYourName.setSize(80, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.PINK);
        lblYourName.setOpaque(true);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90, 5);
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(90, 40);
        
        MyActionListener myActionListener = new MyActionListener();
        
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener((ActionListener) new HelloMe());
        
        
        ActionListener actionListener = new ActionListener(){//Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e){
                System.out.println("Anonymous Class: Action");
            }
        };
        btnHello.addActionListener(actionListener);
        
        JLabel lblHello = new JLabel("Hello...",JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);
        lblHello.setBackground(Color.GREEN);
        lblHello.setOpaque(true);
        
        frmMain.setLayout(null);
        
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lblHello);
        
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                String name = txtYourName.getText();
                lblHello.setText("Hello "+name);
            }
    });
        btnHello.addMouseListener(new MouseListener(){
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Mouse Clicked");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("Mouse Pressed");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("Mouse Clicked");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("Mouse Released");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                System.out.println("Mouse Exited");
            }
            });
       
        
        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMe: Action");
    }
}
